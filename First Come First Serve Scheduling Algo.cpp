#include <bits/stdc++.h>
#define pb push_back
using namespace std;

typedef long long ll;
typedef vector<ll>vll;

const int high = 100;

vll arrivalTime, burstTime;

int main()
{
    ll numberProcess , x , y , i;

    cout << "Enter no of process: ";

    cin >> numberProcess;

    cout << "Enter Arrival Time & Burst Time: " << "\n";

    for(i=0; i<numberProcess; i++)
    {
        cin >> x >> y;

        arrivalTime.push_back(x);
        burstTime.push_back(y);
    }

    cout << "Solution Table: " << "\n";

    ll temp=0 , waitingTime=0 , ternAroundTime=0;

    cout << "Process  --  Start  --  End  --  Waiting Time  --  Turn Around Time" << "\n";

    for(i=0; i<numberProcess ; i++)
    {
        cout << "P" << (i+1) << "\t\t" << temp << "\t" << (temp+burstTime[i]) << "\t\t" << abs(temp-arrivalTime[i]) << "\t\t" << (abs(temp-arrivalTime[i]) + burstTime[i]) << "\n";

        waitingTime += ( abs(temp-arrivalTime[i]) );

        ternAroundTime += ( abs(temp-arrivalTime[i])+burstTime[i] );

        temp = (temp + burstTime[i] );
    }

    double averageWaitingTime = ((waitingTime * 1.0) / numberProcess * 1.0 );
    double averageTernAroundTime = ( (ternAroundTime * 1.0) / numberProcess * 1.0 );

    cout << "\n" <<"\n";

    cout<<"Average waiting time: " << averageWaitingTime << "\n";

    cout<<"Average turn around time: " << averageTernAroundTime << "\n";

    return 0;
}
